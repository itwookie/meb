Micro Event Bus
=====

This project implements a minimalistic, statically allocated event bus intended for mcs.
The implementation is done using a single header and should be compatible with c++11.

### Setup the eventbus:

I recommend exposing a single eventbus instance through a separate header.

Along with the event bus instance, you can declare an enum with all events you expect. 
There are no real requirements to "register" an event type.

```c++
#include "meb.hpp"

MEB::MEB<
	32, // Size of Event Buffer (no more than this ammount of events can be stored on the bus)
	64  // Maximum number of system wide subscriptions
	> eventbus;

// any id but 0 is ok to use
enum EventIDs : MEB::eventid {
	INVALID=0, ExampleEvent, ...
}
```

Don't forget to tick the eventbus within your main loop. 
This will process all events that have been buffered since the last invocation.

```c++
void main() {
	//...
	
	eventbus.tick();
	
	//...
}
```

### Listening to events

You can listen to events using any void(*)(int) method. All events receive an additional int as parameter that, by default, will be 0.
If you do not need the listener anymore you can unhook it again, or register it for a singleshot to begin with.

- `MEB::hook()` returns true if the event was subscribed. This should only fail if you exceede the number of subscribers.
- `MEB::unhook()` returns the amount of listeners removed or true if you removed a specific listener.

```c++
void onExampleEvent(int arg) {
	printf("Event triggered with arg %i\n", arg);
}

//...
	eventbus.hook(ExampleEvent, onExampleEvent /*, singleshot? */);
//...
	eventbus.unhook(ExampleEvent, onExampleEvent); //unhooks just this listener. 
	eventbus.unhook(ExampleEvent); //unhooks all listeners to this event
//...
```

### Firing events

Posting an event is as simple as calling `MEB::post()`. 
The post method returns false iif the event buffer was full and thus the event failed to be added.

```c++
//...
	int arg = 0;
	eventbus.post(ExampleEvent, arg);
//...
```
