#include <iostream>
#include "meb.hpp"

enum events : MEB::eventid { invalid, busReadyEvent, testEvent };
MEB::MEB<10,30> meb;

void onMebReady(int a) {
	std::cout << "MEB is Ready" << std::endl;
}

int main() {
	meb.hook(busReadyEvent, onMebReady);
	meb.tick();
	std::cout << "stepped" << std::endl;
	meb.post(busReadyEvent);
	meb.tick();
	std::cout << "stepped" << std::endl;
	meb.unhook(busReadyEvent);
	int i=1;
	while (meb.post(testEvent,i)) std::cout << "posted " << (i++) << " events" << std::endl;
	while (i-->0) meb.tick();
	std::cout << "stepped" << std::endl;
}