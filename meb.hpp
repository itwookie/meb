/** MicroEventBus: https://gitlab.com/ITWookie/MEB */

#include <cstring>

namespace MEB {
	typedef int eventid; //defined by user, non 0
	/** events receive an argument (alsways, value might not be defined) */
	typedef void(*mebConsumer)(int);
	struct mebEvent {
		eventid event;
		int arg;
	};
	struct mebSubscription {
		eventid event;
		mebConsumer callback;
		bool singleshot;
	};
	template<typename T, int S>
	class RingBuffer {
		private:
			T element[S];
			int write=0;
			int read=0;
			int cap=S;
		public:
			bool isEmpty() { return cap==S; }
			bool isFull() { return cap==0; }
			bool push(T& insert) {
				if (isFull()) return false;
				cap--;
				memcpy(element+write, &insert, sizeof(T));
				if (++write >= S) write=0;
				return true;
			}
			bool pop(T* extract) {
				if (isEmpty()) return false;
				memcpy(extract, element+read, sizeof(T));
				if (++read >= S) read=0;
				cap++;
				return true;
			}
			/** do not free, do not store */
			T* readRaw() {
				if (isEmpty()) return nullptr;
				T* result=element+read;
				if (++read >= S) read=0;
				cap++;
				return result;
			}
			/** do not free, do not store */
			T* writeRaw() {
				if (isFull()) return nullptr;
				cap--;
				T* result=element+write;
				if (++write >= S) read=0;
				return result;
			}
	};

	template<int BUFFER_DEPTH, int MAX_SUBSCRIBERS>
	class MEB {
		private:
			RingBuffer<mebEvent,BUFFER_DEPTH> bus;
			mebSubscription listener[MAX_SUBSCRIBERS];
		public:
			bool post(eventid eid, int arg=0) {
				if (eid==0) return false;
				mebEvent* e = bus.writeRaw();
				if (e == nullptr) return false;
				e->event = eid;
				e->arg = arg;
				return true;
			}
			void tick() {
				mebEvent e;
				while (bus.pop(&e)) {
					for (auto&& it : listener) {
						if (it.event) {
							it.callback(e.arg);
							if (it.singleshot) it.event = 0;
						}
					}
				}
			}
			bool hook(eventid event, mebConsumer callback, bool once=false) {
				int firstempty=-1;
				for (int i=0;i<MAX_SUBSCRIBERS;i++) {
					if (listener[i].event == 0 && firstempty < 0) { firstempty=i; }
					if (listener[i].event == event && listener[i].callback == callback) {
						listener[i].singleshot = once;
						return true; //changed
					}
				}
				if (firstempty < 0) return false; // no more subscribers
				listener[firstempty].event = event;
				listener[firstempty].callback = callback;
				listener[firstempty].singleshot = once;
				return true;
			}
			int unhook(eventid event) {
				if (event == 0) return 0; //shortcut out
				int removed = 0;
				for (int i=0;i<MAX_SUBSCRIBERS;i++) {
					if (listener[i].event == event) {
						listener[i].event = 0;
						removed ++;
					}
				}
				return removed;
			}
			bool unhook(eventid event, mebConsumer callback) {
				for (int i=0;i<MAX_SUBSCRIBERS;i++) {
					if (listener[i].event == event && listener[i].callback == callback) {
						listener[i].event = 0;
						return true;
					}
				}
				return false;
			}
	};
}